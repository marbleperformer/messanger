import sys

from PyQt5.QtWidgets import QApplication

from utility.configuration.configuration import ConfigAttribute, ConfigMetaclass


class ClientConfig(metaclass=ConfigMetaclass):

    plugins_dir = ConfigAttribute('plugins_dir', str, 'plugins')

    plugins = ConfigAttribute('plugins', list, ['base'])

    username = ConfigAttribute('username', str, 'TestUser')


    def __init__(self, path=None):

        if path:

            with open(path, 'r') as file:

                config_json = json.load(file)

                for name, value in config_json.items():

                    setattr(self, name, value)


if __name__ == '__main__':


    app = QApplication(sys.argv)

    app.config = ClientConfig()

    for name in app.config.plugins:

        plugin_full_name =  '.'.join((app.config.plugins_dir, name))

        module = __import__(plugin_full_name, {})

        plugin = module.__getattribute__(name)

        plugin.__plugin__(app)


    sys.exit(app.exec_())

