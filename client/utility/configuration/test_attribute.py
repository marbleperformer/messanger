import pytest

from .configuration import ConfigAttribute


@pytest.fixture
def attribute():

    return ConfigAttribute('name', str)


@pytest.fixture
def config(attribute):

    return type('TestConfig', (), {'attribute':attribute})


def test_init(config):

    assert config.attribute == str()


def test_set(config):

    test_conf = config()

    print(test_conf)

    test_conf.attribute = 'Some value'


@pytest.mark.xfail(raises=TypeError)
def test_set_wrong(config):

    test_conf = config()

    test_conf.attribute = 123
