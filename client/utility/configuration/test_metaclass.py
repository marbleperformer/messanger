import pytest

from .configuration import ConfigMetaclass, ConfigAttribute


@pytest.fixture
def config():

    class TestConfig(metaclass=ConfigMetaclass):

        attr = ConfigAttribute('attr', str, 'test')

    return TestConfig


def test_singleton(config):

    cnf1 = config()

    cnf2 = config()

    assert cnf1 is cnf2


def test_unique_instance(config):

    class NewConfig(metaclass=ConfigMetaclass):

        attr1 = ConfigAttribute('attr1', int)

    assert not config() is NewConfig()


def test_init(config):

    cnf = config()

    cnf.attr = 'test'


def test_update_attribute(config):

    cnf1 = config()

    cnf2 = config()

    cnf2.attr = 'update'

    assert cnf1.attr == 'update'
