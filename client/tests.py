import pytest

from client import get_request_bytes, get_response, handle_response


response_BYTES = b'{"action":"echo", "message":"Some message"}'

response_NOT_BYTES = '"{action":"echo", "message":"Some message"}'

response = {'action':'echo', 'message':'Some message'}


CODE = 200

MESSAGE = 'Some message'

RESPONSE_BYTES = b'{"code": 200, "message": "Some message"}'

RESPONSE_NOT_BYTES = '{"code": 200, "message": "Some message"}'

RESPONSE = {'code':200, 'message':'Some message'}


NOT_FUOND_CODE = 404

INTERNAL_ERROR_CODE = 500

WROWNG_ACTION_response = {"action":"wrongaction", "message":"Some message"}

WRONG_response = b'Some wrong response'


ACTION = 'echo'

CLIENT_OUT = MESSAGE + '\n'


class TestCaseClient:

    def test_get_response(self):

        response = get_response(RESPONSE_BYTES)

        assert response == RESPONSE


    @pytest.mark.xfail(raises=AttributeError)
    def test_not_bytes_get_response(self):

        response = get_response(RESPONSE_NOT_BYTES)


    def test_get_responce_bytest(self):

        request_bytes = get_request_bytes(ACTION, MESSAGE)

        assert isinstance(request_bytes, bytes)


    def test_handle_response(self, capsys):

        handle_response(RESPONSE)

        out, err = capsys.readouterr()

        assert out == CLIENT_OUT
