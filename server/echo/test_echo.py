import pytest

import echo.controllers as controllers


@pytest.fixture
def client_request():

    return {'some client': 'message'}


@pytest.fixture
def response():

    return {'code':200, 'message':{'some client': 'message'}}


class TestCaseEcho:

    def test_echo(self, client_request, response):

        status, address, rsp = controllers.echo(client_request)

        print(response)

        print(rsp)

        assert rsp == response
