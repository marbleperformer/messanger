from utility.database.database import Base

from sqlalchemy import Column, Integer, String, DateTime, Text

class Message(Base):

    __tablename__ = 'messages'

    id = Column(Integer, primary_key=True)

    body = Column(Text)

    sender = Column(String)

    sended = Column(DateTime)

