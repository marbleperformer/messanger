import datetime

from utility.shortcuts.shortcuts import send_all

from utility.logging.logging import trace_request

from utility.database.database import DatabaseManager

from .models import Message


@trace_request
def send_message_to_all(request):

    manager = DatabaseManager()

    session = manager.create_session()

    user = request.get('user')

    body = request.get('message')

    date = datetime.datetime.now()

    message = Message(sender=user, body=body, sended=date)

    session.add(message)

    session.commit()

    return send_all(request)
