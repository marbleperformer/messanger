import os

import sys

import time

import json

import socket

import select

import socketserver


import utility.base.base as base

import utility.managers.managers as managers

import utility.shortcuts.shortcuts as shortcuts

from utility.database.database import DatabaseManager


DEFAULT_HOST = ''

DEFAULT_PORT = 8000

BUFFER_SIZE = 1024


class SimpleHandler(socketserver.BaseRequestHandler):

    def send(self, address, response):

        readers = self.server.get_readers(address)

        closed =self.server.get_closed()

        sockets = self.server.sockets

        r_list, w_list, e_list = select.select([], readers, [])

        for client in w_list:

            try:

                response_bytes = base.response_to_bytes(response)

                client.send(response_bytes)

            except (ConnectionResetError, OSError, ConnectionAbortedError):

                sockets.remove(client)

        list(map(sockets.remove, closed))


    def handle(self):

        sockets = self.server.sockets

        manager = self.server.manager

        if not self.request in sockets:

            sockets.append(self.request)

        request_bytes = self.request.recv(BUFFER_SIZE)

        if request_bytes:

            request = base.bytes_to_request(request_bytes)

            status, address, response = manager.handle_request(request)

            if status == shortcuts.SENDALL:

                address = None

            elif status == shortcuts.SENDBACK:

                address = self.request.getsockname()

            self.send(address, response)


class SimpleSocketServer(socketserver.ThreadingTCPServer):

    def __init__(self, address, handler, manager, *args, **kwargs):

        super(SimpleSocketServer, self).__init__(address, handler, *args, **kwargs)

        self._manager = manager

        self._sockets = list()


    allow_reuse_address = True


    @property
    def manager(self):

        return self._manager


    @property
    def sockets(self):

        return self._sockets


    def get_readers(self, address):

        if address:

            return filter(lambda cln: cln.fileno() != -1 and cln.getsockname() == address, self._sockets)

        else:

            return filter(lambda cln: cln.fileno() != -1, self._sockets)


    def get_closed(self):

        return filter(lambda cln: cln.fileno() == -1, self._sockets)


if __name__ == '__main__':

    manager = DatabaseManager()

    if not os.path.exists('server.db3'):

        manager.init_database()

    try:

        server = SimpleSocketServer((DEFAULT_HOST, DEFAULT_PORT), SimpleHandler, managers.SimpleManager)

        server.serve_forever()
        
    except KeyboardInterrupt:

        sys.exit()
