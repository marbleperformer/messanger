import pytest

from .controllers import not_found, internal_server_error


@pytest.fixture
def clinet_request():

    return {'some':'clinent request'}


class TestCaseControllers:

    def test_not_found(self, clinet_request):

        not_found(clinet_request)


    def test_internal_server_error(self, clinet_request):

        internal_server_error(clinet_request)
