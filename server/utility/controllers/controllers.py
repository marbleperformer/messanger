import utility.shortcuts.shortcuts as shortcuts


NOT_FUOND_ERROR_TEXT = 'Unsupported action.'

INTERNAL_SERVER_ERROR_TEXT = 'Internal sever error.'

PERMISSION_DENIED_ERROR_TEXT = 'PERMISSION DENIED'


def not_found(request):

    return shortcuts.send_back_with_code(404, NOT_FUOND_ERROR_TEXT)


def internal_server_error(request):

    return shortcuts.send_back_with_code(500, INTERNAL_SERVER_ERROR_TEXT)


def permission_denied(request):

    return shortcuts.send_back_with_code(403, )
