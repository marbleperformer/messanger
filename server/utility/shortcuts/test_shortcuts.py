import pytest

from .shortcuts import send, send_back_with_code, send_back, send_all, SEND, SENDBACK, SENDALL


@pytest.fixture
def context():

    return {'message':'some message'}


@pytest.fixture
def address():

    return 'testuser'


@pytest.fixture
def code():

    return 200


class TestCaseShortcuts:


    def test_send(self, address, context):

        status, address, response = send(address, context)

        assert status == SEND


    def test_send_back_with_code(self, code, context):

        status, address, response = send_back_with_code(code, context)

        assert status == SENDBACK


    def test_send_back(self, context):

        status, address, response = send_back(context)

        assert status == SENDBACK


    def test_send_all(self):

        status, address, response = send_all(context)

        assert status == SENDALL
