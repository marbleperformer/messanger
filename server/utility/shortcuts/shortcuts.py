SEND = 'SND'

SENDBACK = 'SNB'

SENDALL = 'SNA'

STATUS_CODES = (SEND, SENDALL, SENDBACK)


def send(address, context):

    message = {'code':201, 'message':context}

    return SEND, address, message


def send_back_with_code(code, context):

    message = {'code':code, 'message':context}

    return SENDBACK, None, message


def send_back(context):

    message = {'code': 200, 'message':context}

    return SENDBACK, None, message


def send_all(context):

    message = {'code':202, 'message':context}

    return SENDALL, None, message