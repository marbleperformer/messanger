from sqlalchemy import create_engine

from sqlalchemy.orm import sessionmaker

from sqlalchemy.ext.declarative import declarative_base

from utility.configuration.configuration import ConfigAttribute, ConfigMetaclass


Base = declarative_base()


class DatabaseConfig(metaclass=ConfigMetaclass):

    connection_string = ConfigAttribute('string', str, 'sqlite:///server.db3')


    def __init__(self, path=None):

        if path:

            with open(path, 'r') as file:

                config_json = json.load(file)

                for name, value in config_json.items():

                    setattr(self, name, value)


class DatabaseManager(metaclass=ConfigMetaclass):

    def __init__(self):

        self._config = DatabaseConfig()

        self._engine = create_engine(self._config.connection_string)

        self._session_class = sessionmaker(bind=self._engine)


    def init_database(self):

        Base.metadata.create_all(self._engine)


    def create_session(self):

        return self._session_class()
