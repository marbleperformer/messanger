import utility.controllers.controllers as controllers

import messages.controllers as msg_ctr

import users.controllers as usr_ctr

import echo.controllers as echo_ctr


ACTIONS = {

    'echo':echo_ctr.echo,
    
    'sendall':msg_ctr.send_message_to_all,

    'auth':usr_ctr.authenticate,

}


class BaseManager:

    def perform_handle(request):

        pass


    @classmethod
    def handle_request(cls, request):

        try:

            action = request['action']

            if action in ACTIONS:

                return cls.perform_handle(request)

            else:

                return controllers.not_found(None)

        except Exception as err:

            return controllers.internal_server_error(None)


class SimpleManager(BaseManager):

    def perform_handle(request):

        action = request['action']

        controller = ACTIONS.get(action)

        message = request['message']

        return controller(message)


class SecuredManager(BaseManager):

    def perform_handle(request):

        action = request['action']

        cache = request.get('cache')

        message = request.get('message')

        controller = ACTIONS.get(action)

        return controller(message)
