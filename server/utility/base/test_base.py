import pytest

import json

from .base import bytes_to_request, response_to_bytes


@pytest.fixture
def request_bytest():

    return b'{"action":"echo", "message":"Some message"}'


@pytest.fixture
def request():

    return {'action':'echo', 'message':'Some message'}


@pytest.fixture
def wrong_request_bytes():

    return b'wrong request bytes'


@pytest.fixture
def string_request():

    return "'code':200, 'message':'Some message'"


@pytest.fixture
def response():

    return {'code':200, 'message':'Some message'}


@pytest.fixture
def string_response():

    return 'string response'


class TestCaseBase:


    def test_bytes_to_request(self, request_bytest, request):

        req = bytes_to_request(request_bytest)

        assert req == request


    @pytest.mark.xfail(raises=TypeError)
    def test_string_bytes_to_request(self, string_request):

        req = bytes_to_request(string_request)


    @pytest.mark.xfail(raises=json.JSONDecodeError)
    def test_wrong_bytes_to_request(self, wrong_request_bytes):

        req = bytes_to_request(wrong_request_bytes)


    def test_response_to_bytes(self, response):

        response_bytes = response_to_bytes(response)

        assert isinstance(response_bytes, bytes)


    @pytest.mark.xfail(raises=TypeError)
    def test_string_response_to_bytes(self, string_response):

        response_to_bytes(string_response)
        