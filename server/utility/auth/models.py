from utility.database.database import Base

from sqlalchemy import Column, Integer, String, DataTime


class User(Base):

    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)

    login = Column(String, unique=True)

    _password = Column(String)


    @property
    def password(self):

        pass


    @password.setter
    def password(self, value):

        pass
